#!/bin/bash

# Does not need git repositories and works in batch.
# Assumes buggy and fixed files are available (e.g. in the bugs/ and fixes/ directories)

# Input: File containing bug ids to report files and corresponding changed line nrs.
# Usage example: bash files_and_line_nrs.sh <file>
# where <file> contains one bug id per line.

BUG_IDS_FILE="$1"

while read bug_id
do
    for buggy_file in $(find bugs/"$bug_id" -type f)
    do
        bug_id_filename="$(cut -d "/" -f2- <<< "$buggy_file")"
        fixed_file=fixes/"$bug_id_filename"
        line_nrs=($(diff --unchanged-line-format="" --new-line-format="%dn: %L" --old-line-format="" "$buggy_file" "$fixed_file" | cut -d ":" -f1))
		for line_nr in "${line_nrs[@]}"
		do
		    echo "$buggy_file" "$line_nr"
		done
    done
done < "$BUG_IDS_FILE"

