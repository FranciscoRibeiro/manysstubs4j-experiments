#!/bin/sh

# Builds the different possibilities for patched lines based on:
# - character stopping criteria (files already generated and placed in STOP_DIR passed as 2nd parameter)
# - character resume criteria (characters considered in this script)

# Input:
# 1st param: Directory with prediction/completion files to read
# 2nd param: Directory with character stopping criteria files to read
# 3rd param: Directory with truncated files to read
# 4th param: Directory to place the generated patched lines files

# Usage: bash scripts/build_patched_lines.sh outs/truncated/ stop/ truncated/ patched_lines/

PREDS_DIR="$1"
STOP_DIR="$2"
TRUNC_DIR="$3"
OUT_PATCH_DIR="$4"

declare -A STOP_RESUME
#STOP_RESUME=([and]="&" [closed_par]=") , &" [comma]=", )" [dq]="\"" [open_par]="(" [or]="|" [closed_curly]="}" [closed_sqr]="]" [dot]="." [open_curly]="{" [open_sqr]="[" [sc]=";" [space]=" " [equal]="=" [plus]="+" [minus]="-" [geq]=">")
STOP_RESUME=([and]="and" [closed_par]="closed_par comma and" [comma]="comma closed_par" [dq]="dq" [open_par]="open_par" [or]="or" [closed_curly]="closed_curly" [closed_sqr]="closed_sqr" [dot]="dot" [open_curly]="open_curly" [open_sqr]="open_sqr" [sc]="sc" [space]="space" [equal]="equal" [plus]="plus" [minus]="minus" [geq]="geq" [leq]="leq")
#STOP_RESUME=([space]="space")

#declare -A CHAR_NAME
#CHAR_NAME=(["&"]="and" [")"]="closed_par" [","]="comma" ["\""]="dq" ["("]="open_par" ["|"]="or" ["}"]="closed_curly" ["]"]="closed_sqr" ["."]="dot" ["{"]="open_curly" ["["]="open_sqr" [";"]="sc" [" "]="space" ["="]="equal" ["+"]="plus" ["-"]="minus" [">"]="geq")

declare -A NAME_CHAR
NAME_CHAR=(["and"]="&" ["closed_par"]=")" ["comma"]="," ["dq"]="\"" ["open_par"]="(" ["or"]="|" ["closed_curly"]="}" ["closed_sqr"]="]" ["dot"]="." ["open_curly"]="{" ["open_sqr"]="[" ["sc"]=";" ["space"]=" " ["equal"]="=" ["plus"]="+" ["minus"]="-" ["geq"]=">" ["leq"]="<")

indexes_of () {
    local str="$1"
    local char="$2"
    local idxs=()
    for (( i=0; i<"${#str}"; i++ ));
    do
        [[ "${str:$i:1}" = "$char" ]] && idxs+=("$i")
    done
    echo "${idxs[@]}"
}

calc_trunc_line () {
    nr_lines="$(wc -l < "$1")"
    #echo "$((nr_lines+1))"
    echo "$nr_lines"
}

continue_pos () {
    local nr_lines="$1"
    local buggy_file="$2"
    local len_trunc="$3"
    local char="$4"
    bug_suffix="$(head -n "$nr_lines" "$buggy_file" | tail -n 1 | tail -c +$len_trunc)"
    idxs=($(indexes_of "$bug_suffix" "$char"))
    for (( i=0; i<${#idxs[@]}; i++ ));
    do
        #idxs[$i]=$((idxs[$i]+$len_trunc+1))
        idxs[$i]=$((idxs[$i]+$len_trunc))
    done
    echo "${idxs[@]}"
}

for proj in $(ls "$PREDS_DIR")
#for proj in okhttp_327de18c95ab972963190945a8b72163417d7406
#for proj in wildfly_5b11b62810156d985ad70527c02708d9c7ecc2a4
#for proj in Activiti_041d650753ff4b846fc86e4266658fc64046bc45
#for proj in ActionBarSherlock_7d7049f9007a940fc7a20f28b706c1144c59223c
#for proj in Activiti_2983b18fd66c6b375d7589f2b78df637151dd9db
#for proj in Activiti_3e8401977adba2a8fdd32c00cd6ce1a932676be8
#for proj in Activiti_4a6c67505d565ea4a2cb58c558a987d53d6ad6bd
#for proj in ActiveAndroid_353110f25bab1a09e08b5466f6af96082b81e261
#for proj in ActiveAndroid_fc685a17d0a2802ba889b6b3219097816253faee
#for proj in graylog2-server_14c2a57c072c6036ba7c9246efb4186fd8fd2448
#for proj in alluxio_fba66d652357d9f116945382b2e3b1e65e295376
#for proj in camel_cdeb155b1c6305dda738ce6eb8d4283b12a535fc
#for proj in ActiveAndroid_8b92dd77aeac147b4cb96d25ad594a8db5cf9b34
#for proj in ActiveAndroid_b4b2c090c26463acff97e869e652170629e3256b
#for proj in Activiti_7753dab97afbc48e87d8d07961c356dbc2885401
do
    for stop_type in ${!STOP_RESUME[@]}
    do
        for resume_char_name in ${STOP_RESUME["$stop_type"]}
        do
			resume_char="${NAME_CHAR["$resume_char_name"]}"
            [[ "$stop_type" = "$resume_char_name" ]] && patched_subdir="$stop_type" || patched_subdir="$stop_type"-"$resume_char_name"
            counter=1
            for pred in $(find "$STOP_DIR"/*"$stop_type/$proj" -name "*\.pred")
            do
                bug_id_filename="$(cut -d "/" -f4- <<< "$pred")"
                mkdir -p "$OUT_PATCH_DIR"/"$patched_subdir/$(dirname "$bug_id_filename")"
                pre_lines_file="$(basename "$pred")"
                pre_lines_file="${pre_lines_file%%.*}.java"
                trunc_line="$(tail -n 1 "$TRUNC_DIR"/"$(dirname $bug_id_filename)"/"$pre_lines_file")"
                len_trunc_line="$(wc -c <<< "$trunc_line")"
                nr_lines_trunc=$(calc_trunc_line "$TRUNC_DIR"/"$(dirname $bug_id_filename)"/"$pre_lines_file")
                buggy_line="$(head -n "$nr_lines_trunc" bugs/"$(dirname $bug_id_filename)"/"$(sed -r 's/^[0-9]+_([0-9]+_)?//' <<< "$pre_lines_file")" | tail -n 1)"
                cont_positions=($(continue_pos "$nr_lines_trunc" bugs/"$(dirname $bug_id_filename)"/"$(sed -r 's/^[0-9]+_([0-9]+_)?//' <<< "$pre_lines_file")" "$len_trunc_line" "$resume_char"))
                for (( cp=0; cp<"${#cont_positions[@]}"; cp++ ));
                do
                    len_continue="${cont_positions[$cp]}"
                    continue_line="$(tail -c +"$len_continue" <<< "$buggy_line")"
                    #exit 0
                    while IFS='' read -r line
                    do
                        line="$(head -c -2 <<< "$line")"
						patched_line_file="$OUT_PATCH_DIR"/"$patched_subdir"/"$(dirname $bug_id_filename)"/"$counter"_"$cp"_"$pre_lines_file"
                        [[ "${trunc_line: -1}" = " " && "${line:0:1}" = " " ]] && echo "yes" && trunc_line="${trunc_line:0: -1}"
                        echo -n "$trunc_line" > "$patched_line_file"
                        echo -n "$line" >> "$patched_line_file"
                        echo "$continue_line" >> "$patched_line_file"
						#exit
                        counter=$((counter+1))
                    done < <(sort -u $pred)
                done
            done
        done
    done
done

