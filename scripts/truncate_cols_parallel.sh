#!/bin/bash

# Does not need git repositories and works in batch.
# Assumes buggy and fixed files are available (e.g. in the bugs/ and fixes/ directories)

# Input: 
# 1st param: File containing buggy file, line nr and column nrs to truncate,
# 2nd param: Directory to place truncated files.
# Usage example: bash truncate_cols_parallel.sh <file> truncated_cols/
# where <file> contains one buggy file, line nr and column nrs per line
# and "truncated_cols" is the directory where truncated files are saved.

BUG_IDS_FILE="$1"
OUT_DIR="$2"

mkdir -p "$OUT_DIR"
parallel -C ' ' bash scripts/helper/truncate_cols_single.sh {1} {2} {3} "$OUT_DIR" < "$BUG_IDS_FILE"

