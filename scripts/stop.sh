#!/bin/sh

# Similar to "skip.sh" but it only performs the STOP step.
# Generates files from predictions/completions based ONLY ON different characters as STOPPING criteria.
# That is, it discards the characters after the occurrence of the STOP character.
# Input:
# 1st param: Directory with completion files to read
# 2nd param: Directory to place the output files

PREDS_DIR="$1"
OUT_STOP_DIR="$2"

declare -A CHARS
CHARS=([and]="\&" [closed_par]=")" [comma]="," [dq]="\"" [open_par]="(" [or]="|" [closed_curly]="}" [closed_sqr]="]" [dot]="\." [open_curly]="{" [open_sqr]="\[" [sc]=";" [space]=" " [equal]="=" [plus]="+" [minus]="\-" [geq]=">" [leq]="<")

stop () {
    local pred_file="$1"
    local stop_char="$2"
    while IFS= read -r line
    do
        grep -q "$stop_char.*$" <<< "$line" && sed "s/$stop_char.*$/$stop_char/"
    done < "$pred_file"
}

for pred in $(find "$PREDS_DIR" -name "*\.pred")
do
    bug_id_filename="$(cut -d "/" -f3- <<< "$pred")"
    for stop_char in "${!CHARS[@]}"
    do
        mkdir -p "$OUT_STOP_DIR"/"$stop_char"/"$(dirname "$bug_id_filename")"
        stop "$pred" "${CHARS["$stop_char"]}" > "$OUT_STOP_DIR"/"$stop_char"/"$bug_id_filename"
    done
done

