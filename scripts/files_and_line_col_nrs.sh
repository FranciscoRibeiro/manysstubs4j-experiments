#!/bin/bash

# Does not need git repositories and works in batch.
# Assumes buggy and fixed files are available (e.g. in the bugs/ and fixes/ directories)

# Input: File containing buggy filenames and buggy line nrs.
# Usage example: bash files_and_line_cols_nrs.sh <file>
# where <file> contains one buggy filename and buggy line nr per line.

BUG_FILES_LINES="$1"

diff_char_pos () {
	local old_line="$1"
	local new_line="$2"
	cmp <(echo "$old_line") <(echo "$new_line") | cut -d "," -f1 | rev | cut -d " " -f1 | rev
}

while IFS=" " read buggy_file line_nr
do
	bug_id_filename="$(cut -d "/" -f2- <<< "$buggy_file")"
	fixed_file=fixes/"$bug_id_filename"
	old_line="$(head -n $line_nr "$buggy_file" | tail -n 1)"
	new_line="$(head -n $line_nr "$fixed_file" | tail -n 1)"
	char_pos="$(diff_char_pos "$old_line" "$new_line")"
	echo "$buggy_file" "$line_nr" "$char_pos"
done < "$BUG_FILES_LINES"

