#!/bin/bash

# Input: File containing buggy filenames, buggy line nrs and column nrs.
# Usage example: bash bug_in_str.sh <file>
# where <file> contains one buggy filename, one buggy line nr and one column nr per line.

BUG_FILES_LINES_COLS="$1"
CODE_TRUNC="code-truncater/code-truncater.jar"

java -jar "$CODE_TRUNC" --is-str "$BUG_FILES_LINES_COLS" "$(pwd)"

