#!/bin/sh

DIR="$1"

#for proj_id in $(ls "$DIR")
for proj_id in "Activiti_14df7cdd9469716d8da651a91df15add6c172f82"
do
	is_fixed=true
	for completion in $(find "$DIR/$proj_id" -name "*\.pred")
	do
		comp_filename="$(basename "$completion")"
		comp_filename=${comp_filename%.txt.pred}
		IFS="_" read -r line_nr filename <<< "$comp_filename"
		no_spaces="$(sed 's/[[:space:]]//g' fix_lines/"$proj_id"/"$line_nr"_$filename.java)"
		escaped="$(sed 's/[^[:alnum:]_-]/\\&/g' <<< "$no_spaces")"
		grep -E "^$escaped" <(sed 's/[[:space:]]//g' "$completion") > /dev/null || { is_fixed=false; break; }
	done
	[[ "$is_fixed" == true ]] && echo "_____ $proj_id"
done

