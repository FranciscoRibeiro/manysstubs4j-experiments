import json
import sys
import subprocess
import itertools

# Expects .json filename as 1st parameter (e.g. sstubs.json or bugs.json)
# Expects action as 2nd parameter (truncate | get_fix_line)
DATASET = sys.argv[1]
ACTION = sys.argv[2]
REPOS_DIR = "projs"

def run(cmd_list):
    subprocess.run(cmd_list)

def repo_path(bug_entry):
    return REPOS_DIR + "/" + bug_entry["projectName"].split(".")[-1]

def bulk_action(commit, bug_entries):
    path = repo_path(bug_entries[0])
    files = " ".join(list(set(map(lambda b: b["commitFile"], bug_entries))))
    #print(commit, " ".join(files))
    run(["bash", "scripts/{}.sh".format(ACTION), path, commit, files])

with open(DATASET) as dataset:
    bugs = json.load(dataset)
    group_by_commit = itertools.groupby(bugs, lambda b: b["commitSHA1"])
    for commit, bug_entries in group_by_commit:
        bulk_action(commit, list(bug_entries))

