#!/bin/sh

BUG_ID_FILE="$1"

is_only_changes () {
    local diffs=($@)
    for d in ${diffs[@]}
    do
        IFS='c' read -r pre pos <<< "$d"
        [[ -z "$pos" || "$pre" != "$pos" ]] && return 1
    done
    return 0
}

while read -r bug_id
do
    diff_types=($(grep -vE "^(<|>|---)" diffs/"$bug_id"/diff.txt))
    is_only_changes ${diff_types[@]} && echo "$bug_id"
done < "$BUG_ID_FILE"

