#!/bin/bash

BUG_IDS_FILE="$1"

for suffix_line_file in $(find fix_lines/ -type f)
do
    bug_id_filename="$(cut -d "/" -f2- <<< "$suffix_line_file")"
    IFS="_" read -r line_nr filename <<< "$(basename "$suffix_line_file")"
    mkdir -p full_fix_lines/"$(dirname "$bug_id_filename")"
    head -n "$line_nr" fixes/"$(dirname "$bug_id_filename")"/"$filename" | tail -n 1 > full_fix_lines/"$bug_id_filename"
done

