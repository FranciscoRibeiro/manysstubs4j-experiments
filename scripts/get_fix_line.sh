#!/bin/bash

deleted_line () {
	local filename="$1"
	line="$(git diff HEAD~1:"$filename" HEAD:"$filename" | grep -E "^-[^-]")"
	echo "${line/#-}"
}

added_line () {
	local filename="$1"
	line="$(git diff HEAD~1:"$filename" HEAD:"$filename" | grep -E "^\+[^\+]")"
	echo "${line/#+}"
}

diff_char_pos () {
	local old_line="$1"
	local new_line="$2"
	cmp <(echo "$old_line") <(echo "$new_line") | cut -d "," -f1 | rev | cut -d " " -f1 | rev
}

REPO_DIR="$1"
COMMIT="$2"
FILENAME="$3"
#LINE="$4"
#OLD_LINE="$5"
#NEW_LINE="$6"

cd "$REPO_DIR";
git checkout "$COMMIT"
git show HEAD~1:"$FILENAME" > previous.java
line_nrs=($(diff --unchanged-line-format="" --new-line-format="%dn: %L" --old-line-format="" previous.java "$FILENAME" | cut -d ":" -f1))
for line_nr in ${line_nrs[@]}
do
    old_line="$(head -n $line_nr previous.java | tail -n 1)"
    new_line="$(head -n $line_nr $FILENAME | tail -n 1)"
    char_pos="$(diff_char_pos "$old_line" "$new_line")"
    trunc_line="$(tail -c +"$char_pos" <<< "$new_line")"
    mkdir -p ../../fix_lines/"$(basename $REPO_DIR)_$COMMIT"
    echo "$trunc_line" > ../../fix_lines/"$(basename $REPO_DIR)_$COMMIT/$line_nr"_"$(basename $FILENAME)"
done
rm previous.java
git checkout -
cd -;

