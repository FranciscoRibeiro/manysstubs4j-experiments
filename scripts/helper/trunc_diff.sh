#!/bin/sh

PROJ_ID="$1"

for file in $(find truncated/"$PROJ_ID" -type f)
do
    bug_id_filename="$(cut -d "/" -f2- <<< "$file")"
	line_nr="$(basename "$bug_id_filename" | cut -d "_" -f1)"
	filename="$(basename "$bug_id_filename" | cut -d "_" -f2-)"
    echo "----- $bug_id_filename -----"
	diff <(head -n "$line_nr" bugs/"$(dirname "$bug_id_filename")/$filename" | tail -n 1) <(head -n "$line_nr" fixes/"$(dirname "$bug_id_filename")/$filename" | tail -n 1)
	echo -n "Intended: "; tail -n 1 "$file"
	echo "Column trunc"
    for trunc_col in $(find truncated_cols/"$PROJ_ID" -name "$line_nr"_*"$filename")
	do
		tail -n 1 "$trunc_col"
		echo
	done
done

