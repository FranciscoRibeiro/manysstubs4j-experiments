#!/bin/sh

# Helper script report what criteria is able to fix a set of bugs in a demo directory
# Input:
# 1st param: demo directory
# Usage example: bash scripts/helper/demo_fix.sh demos1

DEMO_DIR="$1"

for proj in $(ls "$DEMO_DIR"/demos)
do
    fixes=()
    for fix_report in $(find "$DEMO_DIR"/demos/"$proj"/patch_fixes_demo -type f)
    do
        [[ $(wc -l < "$fix_report") -ne 0 ]] && fixes+=("$(basename $fix_report)")
    done
    [[ ${#fixes[@]} -ne 0 ]] && { echo -n "$proj fixed by: " && echo ${fixes[@]}; } || echo "$proj NOT fixed"
done

