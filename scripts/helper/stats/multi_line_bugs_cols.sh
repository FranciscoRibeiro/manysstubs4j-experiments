#!/bin/sh

LINE_INFO="datasets/all_bugs_changed_lines.txt"
COL_INFO="datasets/valid_lines_cols.txt"
BUG_IDS="$1"

MULTI_LINE_COLS=($(grep -f <(grep -f "$BUG_IDS" "$LINE_INFO" | grep -vE " 1$") "$COL_INFO" | cut -d' ' -f3))
IFS=$'\n' UNIQ_MULTI_LINE_COLS=($(sort -nu <<< "${MULTI_LINE_COLS[*]}")); unset IFS

MULTI_LINE=($(grep -f "$BUG_IDS" "$LINE_INFO" | grep -vE " 1$"))

for col_nr in "${UNIQ_MULTI_LINE_COLS[@]}"
do
	echo -n "$col_nr "; tr ' ' '\n' <<< "${MULTI_LINE_COLS[@]}" | grep -x "$col_nr" | wc -l
done

