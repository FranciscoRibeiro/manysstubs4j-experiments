#!/bin/sh

COL_INFO="datasets/valid_lines_cols.txt"

UNIQ_COL_NRS=($(cut -d' ' -f3 "$COL_INFO" | sort -nu))
#UNIQ_COL_NRS=($(grep -f <(sort -u patch_fixes/*) "$COL_INFO" | cut -d' ' -f3 | sort -nu))

for col_nr in "${UNIQ_COL_NRS[@]}"
do
	echo -n "$col_nr "; grep -E " $col_nr$" <(grep -f <(sort -u patch_fixes/*) "$COL_INFO") | wc -l
done

