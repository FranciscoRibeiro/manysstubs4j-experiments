#!/bin/sh

COL_INFO="datasets/valid_lines_cols.txt"

COL_NRS=($(cut -d' ' -f3 "$COL_INFO"))
IFS=$'\n' UNIQ_COL_NRS=($(sort -nu <<< "${COL_NRS[*]}")); unset IFS

#echo "${UNIQ_COL_NRS[@]}"

for col_nr in "${UNIQ_COL_NRS[@]}"
do
	echo -n "$col_nr "; tr ' ' '\n' <<< "${COL_NRS[@]}" | grep -x "$col_nr" | wc -l
done

