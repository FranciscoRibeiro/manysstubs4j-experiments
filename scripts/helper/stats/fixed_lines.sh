#!/bin/sh

LINE_INFO="datasets/valid_lines_cols.txt"

UNIQ_LINE_NRS=($(cut -d' ' -f2 "$LINE_INFO" | sort -nu))
#UNIQ_LINE_NRS=($(grep -f <(sort -u patch_fixes/*) "$LINE_INFO" | cut -d' ' -f2 | sort -nu))

for line_nr in "${UNIQ_LINE_NRS[@]}"
do
	echo -n "$line_nr "; grep -E " $line_nr " <(grep -f <(sort -u patch_fixes/*) "$LINE_INFO") | wc -l
done

