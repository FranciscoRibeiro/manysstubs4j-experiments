#!/bin/sh

LINE_INFO="datasets/valid_lines_cols.txt"

LINE_NRS=($(cut -d' ' -f2 "$LINE_INFO"))
IFS=$'\n' UNIQ_LINE_NRS=($(sort -nu <<< "${LINE_NRS[*]}")); unset IFS

for line_nr in "${UNIQ_LINE_NRS[@]}"
do
	echo -n "$line_nr "; tr ' ' '\n' <<< "${LINE_NRS[@]}" | grep -x "$line_nr" | wc -l
done

