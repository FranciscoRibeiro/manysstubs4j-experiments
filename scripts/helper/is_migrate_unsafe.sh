#!/bin/sh

#Checks if a project's old directory structure (i.e., without full path)
#can be safely migrated to the new directory structure (i.e. with full path).
#Reports unsafe bug ids.

BUG_IDS_FILE="$1"

while read bug_id
do
    for bug_file in $(find bugs/"$bug_id" -type f)
    do
        bug_file_basename="$(basename "$bug_file")"
        matches=($(find bugs/"$bug_id" -name "*$bug_file_basename"))
        [[ ${#matches[@]} -eq 1 ]] || { echo "$bug_id"; break; }
    done
done < "$BUG_IDS_FILE"

