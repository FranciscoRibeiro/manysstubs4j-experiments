#!/bin/sh

BUG_IN_STR="datasets/bug_in_str.txt"
VALID_BUG_IDS="datasets/valid_match_only_changes_bug_ids.txt"
NEW_VALID="datasets/valid_match_only_changes_bug_ids_no_str.txt"

grep -vf <(cut -d "/" -f2 "$BUG_IN_STR" | sort -u) "$VALID_BUG_IDS" > "$NEW_VALID"

