#!/bin/sh

total=0
count=0
while read nr
do
	total=$((total+nr))
	count=$((count+1))
done < /dev/stdin

echo "$total/$count" | bc -l

