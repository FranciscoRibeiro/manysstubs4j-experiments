#!/bin/sh

BUG_IDS_FILE="$1"

while read bug_id
do
    for txt_file in $(find outs/truncated_unsafe/"$bug_id" -name "*\.txt")
    do
        filename="$(basename -s ".txt" "$txt_file" | cut -d "_" -f2-).java"
        src_file="$(find bugs/"$bug_id" -name "$filename")"
        dir_struct="$(dirname "$src_file" | cut -d "/" -f3-)"
        mkdir -p new_outs/truncated/"$bug_id/$dir_struct"
        cp "$txt_file" new_outs/truncated/"$bug_id/$dir_struct"
        cp "$txt_file".pred new_outs/truncated/"$bug_id/$dir_struct"
    done
done < "$BUG_IDS_FILE"

