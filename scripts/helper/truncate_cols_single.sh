#!/bin/bash

# Does not need git repositories and works in batch.
# Assumes buggy and fixed files are available (e.g. in the bugs/ and fixes/ directories)

# Input: 
# 1st param: buggy file,
# 2nd param: line nr,
# 3rd param: column nrs to truncate,
# 4th param: directory to place truncated files.

# Intended to be used by truncate_cols.sh

BUGGY_FILE="$1"
LINE_NR="$2"
COL_NRS="$3"
OUT_DIR="$4"

bug_id_filename="$(cut -d "/" -f2- <<< "$BUGGY_FILE")"
mkdir -p "$OUT_DIR/$(dirname "$bug_id_filename")"
for c_nr in $(tr "," " " <<< "$COL_NRS")
do
	truncated_filename="$OUT_DIR/$(dirname "$bug_id_filename")/$LINE_NR"_"$c_nr"_"$(basename "$bug_id_filename")"
	head -n $((LINE_NR-1)) "$BUGGY_FILE" > "$truncated_filename"
	head -n "$LINE_NR" "$BUGGY_FILE" | tail -n 1 | head -c +"$c_nr" >> "$truncated_filename"
        echo >> "$truncated_filename"
done

