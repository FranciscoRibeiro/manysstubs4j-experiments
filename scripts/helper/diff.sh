#!/bin/sh

PROJ_ID="$1"

for file in $(find bugs/"$PROJ_ID" -type f)
do
    bug_id_filename="$(cut -d "/" -f2- <<< "$file")"
    echo "----- $bug_id_filename -----"
    diff "$file" fixes/"$bug_id_filename"
done

