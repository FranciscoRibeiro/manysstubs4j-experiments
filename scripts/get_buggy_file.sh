#!/bin/bash

REPO_DIR="$1"
COMMIT="$2"
FILENAME="$3"

cd "$REPO_DIR";
git checkout "$COMMIT"
mkdir -p ../../bugs/"$(basename $REPO_DIR)_$COMMIT"
git show HEAD~1:"$FILENAME" > ../../bugs/"$(basename $REPO_DIR)_$COMMIT"/"$(basename $FILENAME)"
git checkout -
cd -;

