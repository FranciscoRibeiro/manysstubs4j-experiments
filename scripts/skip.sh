#!/bin/sh

# Generates files from predictions/completions based on different characters as SKIPPING criteria.
# That is, it skips the first characters until the occurrence of the skip character.
# Input:
# 1st param: Directory with completion files to read
# 2nd param: Directory to place the output files

PREDS_DIR="$1"
OUT_SKIP_DIR="$2"

declare -A CHARS
CHARS=([and]="\&" [closed_par]=")" [comma]="," [dq]="\"" [open_par]="(" [or]="|" [closed_curly]="}" [closed_sqr]="]" [dot]="\." [open_curly]="{" [open_sqr]="\[" [sc]=";" [space]=" " [equal]="=" [plus]="+" [minus]="\-" [geq]=">" [leq]="<")

skip_stop () {
    local pred_file="$1"
    local skip_char="$2"
    local stop_char="$3"
    while IFS= read -r line
    do
        { grep -q "^[^$skip_char]*$skip_char" <<< "$line" || grep -q "$stop_char.*$" <<< "$line"; } && \
            { sed "s/^[^$skip_char]*$skip_char/$skip_char/" <<< "$line" | sed "s/$stop_char.*$/$stop_char/"; }
    done < "$pred_file"
}

for pred in $(find "$PREDS_DIR" -name "*\.pred")
do
    bug_id_filename="$(cut -d "/" -f3- <<< "$pred")"
    for skip_char in "${!CHARS[@]}"
    do
        for stop_char in "${!CHARS[@]}"
        do
            mkdir -p "$OUT_SKIP_DIR"/"$skip_char"-"$stop_char"/"$(dirname "$bug_id_filename")"
            skip_stop "$pred" "${CHARS["$skip_char"]}" "${CHARS["$stop_char"]}" > "$OUT_SKIP_DIR"/"$skip_char"-"$stop_char"/"$bug_id_filename"
        done
    done
done


#for pred in $(find "$PREDS_DIR" -name "*\.pred")
#do
#    bug_id_filename="$(cut -d "/" -f3- <<< "$pred")"
#    
#    mkdir -p "$OUT_SKIP_DIR"/closed_brk/"$(dirname "$bug_id_filename")"
#    #sed 's/^.*>/>/' "$pred" | sed 's/>.*$/>/' > "$OUT_SKIP_DIR"/closed_brk/"$bug_id_filename" #skip until first closing bracket and cut after it
#    skip_single "$pred" ">" > "$OUT_SKIP_DIR"/closed_brk/"$bug_id_filename"
#
#    #mkdir -p "$OUT_SKIP_DIR"/closed_sqr/"$(dirname "$bug_id_filename")"
#    #sed 's/].*$/]/' "$pred" > "$OUT_SKIP_DIR"/closed_sqr/"$bug_id_filename"
#
#    #mkdir -p "$OUT_SKIP_DIR"/dq/"$(dirname "$bug_id_filename")"
#    #sed 's/".*$/"/' "$pred" > "$OUT_SKIP_DIR"/dq/"$bug_id_filename"
#
#    #mkdir -p "$OUT_SKIP_DIR"/sc/"$(dirname "$bug_id_filename")"
#    #sed 's/;.*$/;/' "$pred" > "$OUT_SKIP_DIR"/sc/"$bug_id_filename"
#
#    #mkdir -p "$OUT_SKIP_DIR"/dot/"$(dirname "$bug_id_filename")"
#    #sed 's/\..*$/\./' "$pred" > "$OUT_SKIP_DIR"/dot/"$bug_id_filename"
#
#    #mkdir -p "$OUT_SKIP_DIR"/comma/"$(dirname "$bug_id_filename")"
#    #sed 's/,.*$/,/' "$pred" > "$OUT_SKIP_DIR"/comma/"$bug_id_filename"
#
#    #mkdir -p "$OUT_SKIP_DIR"/closed_curly/"$(dirname "$bug_id_filename")"
#    #sed 's/}.*$/}/' "$pred" > "$OUT_SKIP_DIR"/closed_curly/"$bug_id_filename"
#
#    #mkdir -p "$OUT_SKIP_DIR"/open_par/"$(dirname "$bug_id_filename")"
#    #sed 's/(.*$/(/' "$pred" > "$OUT_SKIP_DIR"/open_par/"$bug_id_filename"
#
#    #mkdir -p "$OUT_SKIP_DIR"/open_sqr/"$(dirname "$bug_id_filename")"
#    #sed 's/\[.*$/\[/' "$pred" > "$OUT_SKIP_DIR"/open_sqr/"$bug_id_filename"
#
#    #mkdir -p "$OUT_SKIP_DIR"/open_curly/"$(dirname "$bug_id_filename")"
#    #sed 's/{.*$/{/' "$pred" > "$OUT_SKIP_DIR"/open_curly/"$bug_id_filename"
#
#    #mkdir -p "$OUT_SKIP_DIR"/and/"$(dirname "$bug_id_filename")"
#    #sed 's/\&.*$/\&/' "$pred" > "$OUT_SKIP_DIR"/and/"$bug_id_filename"
#
#    #mkdir -p "$OUT_SKIP_DIR"/or/"$(dirname "$bug_id_filename")"
#    #sed 's/|.*$/|/' "$pred" > "$OUT_SKIP_DIR"/or/"$bug_id_filename"
#
#    #mkdir -p "$OUT_SKIP_DIR"/plus/"$(dirname "$bug_id_filename")"
#    #sed 's/\+.*$/\+/' "$pred" > "$OUT_SKIP_DIR"/plus/"$bug_id_filename"
#
#    #mkdir -p "$OUT_SKIP_DIR"/minus/"$(dirname "$bug_id_filename")"
#    #sed 's/-.*$/-/' "$pred" > "$OUT_SKIP_DIR"/minus/"$bug_id_filename"
#
#    #mkdir -p "$OUT_SKIP_DIR"/equal/"$(dirname "$bug_id_filename")"
#    #sed 's/=.*$/=/' "$pred" > "$OUT_SKIP_DIR"/equal/"$bug_id_filename"
#
#    #mkdir -p "$OUT_SKIP_DIR"/space/"$(dirname "$bug_id_filename")"
#    #sed 's/ .*$/ /' "$pred" > "$OUT_SKIP_DIR"/space/"$bug_id_filename"
#done

