#!/bin/sh

# Check every character stopping criteria to see if any patch is able to fix a bug
# Input:
# 1st param: Directory with patched files to read
# 2nd param: Directory to store information about patches that fix a bug

# Usage: bash scripts/check_all_stop_types.sh patched_lines/ patch_fixes/

PATCH_DIR="$1"
OUT_FIXES_DIR="$2"

mkdir -p "$OUT_FIXES_DIR"
ls "$PATCH_DIR" | xargs -I@ bash -c "bash scripts/check_if_patches_fix.sh @ full_fix_lines/ "$PATCH_DIR" > $OUT_FIXES_DIR/@.txt"

