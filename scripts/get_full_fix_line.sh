#!/bin/bash

REPO_DIR="$(basename "$1")"
COMMIT="$2"

for suffix_line_file in $(find fix_lines/"$REPO_DIR"_"$COMMIT" -type f)
do
    IFS="_" read -r line_nr filename <<< "$(basename "$suffix_line_file")"
    mkdir -p full_fix_lines/"$REPO_DIR"_"$COMMIT"
    head -n "$line_nr" fixes/"$REPO_DIR"_"$COMMIT"/"$filename" | tail -n 1 > full_fix_lines/"$REPO_DIR"_"$COMMIT"/"$(basename "$suffix_line_file")"
done

