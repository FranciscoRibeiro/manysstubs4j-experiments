#!/bin/sh

while IFS=' ' read -r bug_id nr_changed_lines
do
    true_nr_changed_lines="$(grep "$bug_id" datasets/all_bugs_changed_lines.txt | cut -d " " -f2)"
    [[ $true_nr_changed_lines -eq $nr_changed_lines ]] || echo "$bug_id"
done < datasets/extracted_bugs_changed_lines.txt

