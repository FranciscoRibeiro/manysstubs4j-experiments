import json
import sys
import subprocess
import itertools

# Expects .json filename as 1st parameter (e.g. sstubs.json or bugs.json)
# Expects action as 2nd parameter (truncate | get_fix_line)
DATASET = sys.argv[1]

with open(DATASET) as dataset:
    bugs = json.load(dataset)
    group_by_commit = itertools.groupby(bugs, lambda b: b["commitSHA1"])
    for commit, bug_entries in group_by_commit:
        bug_entries_list = list(bug_entries)
        print(bug_entries_list[0]["projectName"].split(".")[-1] + "_" + commit, len(bug_entries_list))

