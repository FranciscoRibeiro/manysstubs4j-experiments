#!/bin/sh

# Check if any patch is able to fix a bug for a specific character stopping criteria
# Input:
# 1st param: Character stopping criteria
# 2nd param: Directory with full fix lines to read
# 3rd param: Directory with patched lines to read

# Usage: bash scripts/check_if_patches_fix.sh and full_fix_lines/ patched_lines/

STOP_TYPE="$1"
FULL_FIX_LINES_DIR="$2"
PATCH_DIR="$3"

check_with_tolerance () {
    local patched_line="$1"
    local fixed_line="$2"
    local byte_diff="$(cmp -bl <(echo "$patched_line") <(echo "$fixed_line") 2> /dev/null)"
    local nr_byte_diffs="$(wc -l <<< "$byte_diff")"
    [[ "$nr_byte_diffs" -eq 1 && "${byte_diff: -1}" == ")" ]] && return 0
}

logerr () {
    echo "$@" >&2
}

report_no_fix () {
    local bug_id="$1"
    local line_nr="$2"
    logerr "$bug_id line not fixed:$line_nr"
}

report_fix () {
    local bug_id="$1"
    local line_nr="$2"
    local patch_file="$3"
    logerr "$bug_id $line_nr fixed by:$patch_file"
}

#for proj in $(ls "$FULL_FIX_LINES_DIR")
for proj in $(ls "$PATCH_DIR/$STOP_TYPE")
#for proj in "Activiti_3e8401977adba2a8fdd32c00cd6ce1a932676be8"
#for proj in "Activiti_4a6c67505d565ea4a2cb58c558a987d53d6ad6bd"
#for proj in "atmosphere_94342108c55078c0e9c94b8d9aa2a3e65176f1fb"
#for proj in "guava_0f08fad6b0c7f029bdb8b9a4382ed1c37b9095dd"
do
    proj_is_fixed="true"
    for line_fix_file in $(find "$FULL_FIX_LINES_DIR"/"$proj" -type f)
    do
        bug_id_filename="$(cut -d "/" -f2- <<< "$line_fix_file")"
        line_nr="$(cut -d "_" -f1 <<< "$(basename "$line_fix_file")")"
        filename="$(cut -d "_" -f2 <<< "$(basename "$line_fix_file")")"
        fixed_line="$(cat "$FULL_FIX_LINES_DIR"/"$bug_id_filename")"
        line_is_fixed="false"
        for line_patch_file in $(find "$PATCH_DIR"/"$STOP_TYPE"/"$proj" -regextype egrep -regex ".*_$line_nr(_[0-9]+)?_$filename")
        #for line_patch_file in $(find "$PATCH_DIR"/*/"$proj" -regextype egrep -regex ".*_$line_nr(_[0-9]+)?_$filename")
        do
            patched_line="$(cat "$line_patch_file")"
            [[ "$(tr -d '[:space:]' <<< "$fixed_line")" == "$(tr -d '[:space:]' <<< "$patched_line")" ]] \
            || check_with_tolerance "$patched_line" "$fixed_line" && { line_is_fixed="true"; report_fix "$proj" "$line_nr" "$line_patch_file"; break; }
        done
        [[ "$line_is_fixed" == "false" ]] && { proj_is_fixed="false"; report_no_fix "$proj" "$line_nr"; break; }
    done
    [[ "$proj_is_fixed" == "true" ]] && echo "$proj"
done

