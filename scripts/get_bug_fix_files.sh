#!/bin/bash

REPO_DIR="$1"
COMMIT="$2"
FILENAMES="$3"

echo "$REPO_DIR"
echo "$COMMIT"

cd "$REPO_DIR" || exit 1;
git checkout -f "$COMMIT" || { cd - > /dev/null; exit 1; }
mkdir -p ../../bugs/"$(basename $REPO_DIR)_$COMMIT"
mkdir -p ../../fixes/"$(basename $REPO_DIR)_$COMMIT"

for filename in ${FILENAMES[@]}
do
    mkdir -p ../../bugs/"$(basename $REPO_DIR)_$COMMIT"/"$(dirname $filename)"
    git show HEAD~1:"$filename" > ../../bugs/"$(basename $REPO_DIR)_$COMMIT"/"$filename"
    mkdir -p ../../fixes/"$(basename $REPO_DIR)_$COMMIT"/"$(dirname $filename)"
    git show HEAD:"$filename" > ../../fixes/"$(basename $REPO_DIR)_$COMMIT"/"$filename"
done

git checkout -f -
cd - > /dev/null;

