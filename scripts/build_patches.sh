#!/bin/sh

calc_trunc_line () {
    nr_lines="$(wc -l < "$1")"
    echo "$((nr_lines+1))"
}

continue_pos () {
    local nr_lines="$1"
    local buggy_file="$2"
    local len_trunc="$3"
    #bug_suffix="$(head -n "$nr_lines" "$buggy_file" | tail -n 1 | tail -c +$((len_trunc+1)))"
    bug_suffix="$(head -n "$nr_lines" "$buggy_file" | tail -n 1 | tail -c +$len_trunc)"
    cut_closed_par="${bug_suffix/)*/}"
    closed_par_pos="${#cut_closed_par}"
    echo "$((len_trunc+1+closed_par_pos))"
}

#for proj in $(ls outs/truncated)
#for proj in Activiti_041d650753ff4b846fc86e4266658fc64046bc45
for proj in ActionBarSherlock_7d7049f9007a940fc7a20f28b706c1144c59223c
#for proj in Activiti_2983b18fd66c6b375d7589f2b78df637151dd9db
#for proj in Activiti_3e8401977adba2a8fdd32c00cd6ce1a932676be8
do
    for pred in $(find stop/first_closed_par/"$proj" -name "*\.pred")
    do
        pre_lines_file="$(basename "$pred")"
        pre_lines_file="${pre_lines_file%%.*}.java"
        trunc_line="$(tail -n 1 truncated/"$proj"/"$pre_lines_file")"
        len_trunc_line="$(wc -c <<< "$trunc_line")"
        nr_lines_trunc=$(calc_trunc_line truncated/"$proj"/"$pre_lines_file")
        buggy_line="$(head -n "$nr_lines_trunc" bugs/"$proj"/"$(cut -d "_" -f2- <<< "$pre_lines_file")" | tail -n 1)"
        len_continue=$(continue_pos "$nr_lines_trunc" bugs/"$proj"/"$(cut -d "_" -f2- <<< "$pre_lines_file")" "$len_trunc_line")
        continue_line="$(tail -c +"$len_continue" <<< "$buggy_line")"
        exit 0
        counter=1
        while IFS='' read -r line
        do
            cp truncated/"$proj"/"$pre_lines_file" patches/"$proj"/"$counter"_"$pre_lines_file"
            echo -n "$line" >> patches/"$proj"/"$counter"_"$pre_lines_file"
            echo "$continue_line" >> patches/"$proj"/"$counter"_"$pre_lines_file"
            tail -n +"$((nr_lines_trunc+1))" bugs/"$proj"/"$(cut -d "_" -f2- <<< "$pre_lines_file")" >> patches/"$proj"/"$counter"_"$pre_lines_file"
            counter=$((counter+1))
        done < <(sort -u $pred)
    done
done

