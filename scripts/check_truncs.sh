#!/bin/sh

for proj in $(ls truncated_cols)
do
	for trunc in $(find truncated/"$proj" -type f)
	do
		trunc_filename="$(basename "$trunc")"
		directory="$(cut -d "/" -f2 <<< "$(dirname "$trunc")")"
		line_nr="$(cut -d "_" -f1 <<< "$trunc_filename")"
		filename="$(cut -d "_" -f2 <<< "$trunc_filename")"
		truncs_equal="false"
	    for trunc_col in $(find truncated_cols/"$directory" -name "$line_nr"_"*"_"$filename")
		do
			diff -Bb "$trunc" "$trunc_col" > /dev/null && { truncs_equal=true; break; }
		done
		[[ "$truncs_equal" == "false" ]] && echo "$proj"
	done
done

