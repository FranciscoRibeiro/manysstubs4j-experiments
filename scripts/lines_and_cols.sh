#!/bin/bash

# Input: File containing bug ids.
# Usage example: bash lines_and_cols.sh <file>
# where <file> contains one bug id per line.

BUG_IDS="$1"
BUGS_LINES="datasets/all_bugs_changed_lines.txt"
FILES_COLS="datasets/valid_match_only_changes_cols_cut.txt"

while read bug_id
do
	bug_id_lines="$(grep "$bug_id" "$BUGS_LINES")"
	cols_count=0
	while IFS=" " read file line_nr cols
	do
		IFS="," read -ra col_nrs <<< "$cols"
		cols_count=$((cols_count+${#col_nrs[@]}))
	done < <(grep "$bug_id" "$FILES_COLS")
	echo "$bug_id_lines" "$cols_count"
done < "$BUG_IDS"

