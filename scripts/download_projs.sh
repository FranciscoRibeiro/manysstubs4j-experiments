#!/bin/sh

while read proj_id
do
    echo "$proj_id"
    git clone https://github.com/"$proj_id" projs/"$(basename "$proj_id")"
done < "datasets/repo_urls.txt"

