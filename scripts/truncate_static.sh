#!/bin/bash

# Does not need git repositories and works in batch.
# Assumes buggy and fixed files are available (e.g. in the bugs/ and fixes/ directories)

# Input: File containing bug ids to truncate.
# Usage example: bash truncate_static.sh <file>
# where <file> contains one bug id per line.

deleted_line () {
	local filename="$1"
	line="$(git diff HEAD~1:"$filename" HEAD:"$filename" | grep -E "^-[^-]")"
	echo "${line/#-}"
}

added_line () {
	local filename="$1"
	line="$(git diff HEAD~1:"$filename" HEAD:"$filename" | grep -E "^\+[^\+]")"
	echo "${line/#+}"
}

diff_char_pos () {
	local old_line="$1"
	local new_line="$2"
	cmp <(echo "$old_line") <(echo "$new_line") | cut -d "," -f1 | rev | cut -d " " -f1 | rev
}

BUG_IDS_FILE="$1"

while read bug_id
do
    for buggy_file in $(find bugs/"$bug_id" -type f)
    do
        bug_id_filename="$(cut -d "/" -f2- <<< "$buggy_file")"
        fixed_file=fixes/"$bug_id_filename"
        line_nrs=($(diff --unchanged-line-format="" --new-line-format="%dn: %L" --old-line-format="" "$buggy_file" "$fixed_file" | cut -d ":" -f1))
        for line_nr in ${line_nrs[@]}
        do
            pre_lines="$(head -n $((line_nr-1)) "$buggy_file")"
            old_line="$(head -n $line_nr "$buggy_file" | tail -n 1)"
            new_line="$(head -n $line_nr "$fixed_file" | tail -n 1)"
            char_pos="$(diff_char_pos "$old_line" "$new_line")"
            trunc_line="$(head -c "$((char_pos-1))" <<< "$new_line")"
            mkdir -p truncated/"$(dirname "$bug_id_filename")"
            echo "$pre_lines" > truncated/"$(dirname "$bug_id_filename")"/"$line_nr"_"$(basename "$bug_id_filename")"
            echo "$trunc_line" >> truncated/"$(dirname "$bug_id_filename")"/"$line_nr"_"$(basename "$bug_id_filename")"
        done
    done
done < "$BUG_IDS_FILE"

