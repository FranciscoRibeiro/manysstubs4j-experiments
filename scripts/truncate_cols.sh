#!/bin/bash

# Does not need git repositories and works in batch.
# Assumes buggy and fixed files are available (e.g. in the bugs/ and fixes/ directories)

# Input: File containing buggy file, line nr and column nrs to truncate.
# Usage example: bash truncate_cols.sh <file>
# where <file> contains one buggy file, line nr and column nrs per line.

BUG_IDS_FILE="$1"

while IFS=" " read buggy_file line_nr col_nrs
do
    bug_id_filename="$(cut -d "/" -f2- <<< "$buggy_file")"
	mkdir -p truncated_cols/"$(dirname "$bug_id_filename")"
	for c_nr in $(tr "," " " <<< "$col_nrs")
	do
		truncated_filename="truncated_cols/$(dirname "$bug_id_filename")/$line_nr"_"$c_nr"_"$(basename "$bug_id_filename")"
        head -n $((line_nr-1)) "$buggy_file" > "$truncated_filename"
		head -n "$line_nr" "$buggy_file" | tail -n 1 | head -c +"$c_nr" >> "$truncated_filename"
	done
done < "$BUG_IDS_FILE"

