#!/bin/bash

# Extracts only the predictions/completions from the generated outputs.
# Extracted predictions/completions are placed alongside the same files with extension .txt.pred

# Input: Directory with completion files to read

# Usage: bash scripts/extract_preds.sh outs/

DIR="$1"

for out in $(find "$DIR" -name "*\.txt")
do
    tail -n +3 "$out" > "$out.pred"
done

