#!/bin/sh

#Input:
#1. directory with file to complete (e.g. truncated/)

DIR="$1"

things=("truncated/alluxio_9883dbd78a284e0b3322bdbc4ff19980e1de7cf7/93_NettyChannelPool.java" "truncated/hive_96f7960c790fadb6bbc9d81fe095288241d86d8c/1348_StatsUtils.java" "truncated/hive_96f7960c790fadb6bbc9d81fe095288241d86d8c/1352_StatsUtils.java" "truncated/alluxio_47a6d84129e89290cb10c9bac2cc07a65628886e/165_ExceptionMessage.java")

echo -n "${things[@]}" | parallel -d ' ' -j 4 -I@ bash complete.sh @

#find "$DIR" -type f | head -n 4 | xargs -I@ bash complete.sh @

#for filename in $(find "$DIR" -type f | head -n 4)
#do
#	echo "$filename"
#	continue
#	proj="$(cut -d "/" -f2 <<< "$filename")"
#	name="$(basename "$filename")"
#	name=${name%.java}
#	mkdir -p outs/truncated/"$proj"
#	echo "$filename"
#	python3.9 -u new_main.py "$filename" > outs/truncated/"$proj/$name.txt"
#done

