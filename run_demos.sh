#!/bin/sh

mkdir -p demos_times/
mkdir -p demos_logs/

while IFS=' ' read -r bug_id lines
do
    echo "$bug_id"
    { time bash fix.sh "$bug_id" &> demos_logs/"$bug_id".log; } &> demos_times/"$bug_id".txt
done < "$1"

