import sys
import torch
import difflib
import os
import random
import torch
import numpy
from transformers import AutoTokenizer, AutoModelForCausalLM

def write_to_file(tokens, completions, filepath):
    os.makedirs(os.path.dirname(filepath), exist_ok=True)
    f = open(filepath, "a")
    f.write(tokens + "\n")
    f.write("-------------------------------\n")
    for c in completions:
        f.write(c + "\n")
    f.close()

def load():
    CHECKPOINT = "microsoft/CodeGPT-small-java-adaptedGPT2"
    tokenizer = AutoTokenizer.from_pretrained(CHECKPOINT, local_files_only=True)
    #tokenizer = AutoTokenizer.from_pretrained(CHECKPOINT)
    model = AutoModelForCausalLM.from_pretrained(CHECKPOINT, local_files_only=True)
    #model = AutoModelForCausalLM.from_pretrained(CHECKPOINT)
    return tokenizer, model

def read_lines(filepath, clear_indents=False):
    lines = open(filepath, "r", errors="replace").readlines()
    if(clear_indents):
        return list(map(lambda l: l.strip(), lines))
    else:
        return lines

def str_content(filepath):
    return "".join(read_lines(filepath, True))

def get_last_tokens(tokenizer, seq, nr_last):
    return tokenizer.encode(seq, return_tensors="pt")[0][-nr_last:].unsqueeze(0)

def complete(tokenizer, model, tokens):
    completions = []
    try:
        #completions = model.generate(tokens, max_length=len(tokens[0])+20, do_sample=True)
        completions = model.generate(tokens, max_length=len(tokens[0])+20, do_sample=True, top_k=0, top_p=0.92)
        #completions = model.generate(tokens, max_length=len(tokens[0])+1, do_sample=True)
        #completions = model.generate(tokens, max_length=len(tokens[0])+20, num_beams=5, num_return_sequences=5, do_sample=True)
    except RuntimeError as e:
        print("Could not generate completions")

    decoded_completions = []
    for i, c in enumerate(completions):
        decoded = tokenizer.decode(c[-20:])
        #decoded = tokenizer.decode(c[-2:])
        #print(i, decoded)
        #output(decoded)
        decoded_completions.append(decoded)
    return decoded_completions

def calc_output_file(out_dir, filepath):
    return os.path.join(out_dir, os.path.splitext(filepath)[0] + ".txt")

def bulk_process(directory, tokenizer, model, out_dir):
    for root, dirs, files in os.walk(directory):
        for f in files:
            filepath = os.path.join(root, f)
            print(filepath)
            tokens_used, completions = complete_file(filepath, tokenizer, model)
            output_file = calc_output_file(out_dir, filepath)
            write_to_file(tokens_used, completions, output_file)

def complete_file(filepath, tokenizer, model):
    str_seq = str_content(filepath)
    tokens = get_last_tokens(tokenizer, str_seq, 1000)
    tokens_used = tokenizer.decode(tokens[0])
    random.seed(18)
    numpy.random.seed(18)
    torch.manual_seed(18)
    completions = []
    for i in range(1,11):
        completions += complete(tokenizer, model, tokens)
    return tokens_used, completions

def main():
    tokenizer, model = load()
    if(sys.argv[1] == "--bulk"):
        bulk_process(sys.argv[2], tokenizer, model, sys.argv[3])
    else:
        tokens_used, completions = complete_file(sys.argv[1], tokenizer, model)
        print(tokens_used)
        print("-------------------------------")
        for c in completions:
            print(c)

if __name__ == "__main__":
    main()

