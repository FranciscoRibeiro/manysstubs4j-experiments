#!/bin/sh

BUG_ID="$1"

FILES_LINE_NRS="datasets/valid_match_only_changes_files_and_line_nrs.txt"
CODE_TRUNC="code-truncater/code-truncater.jar"

files_and_line_nrs () {
    grep "$BUG_ID" "$FILES_LINE_NRS"
}

calc_trunc_cols () {
	java -jar "$CODE_TRUNC" --bulk <(files_and_line_nrs) "$(pwd)"
}

truncate_files () {
	bash scripts/truncate_cols_parallel.sh <(calc_trunc_cols) truncated_demo/
}

complete () {
    python3.9 complete.py --bulk truncated_demo/ outs_demo/
	bash scripts/extract_preds.sh outs_demo/
}

build_patches () {
	#bash scripts/stop.sh outs_demo/ stop_demo/
	bash -x scripts/skip.sh outs_demo/ skip_demo/
	#bash scripts/build_patched_lines.sh outs_demo/truncated_demo/ stop_demo/ truncated_demo/ patched_lines_demo/
	bash -x scripts/build_patched_lines.sh outs_demo/truncated_demo/ skip_demo/ truncated_demo/ patched_lines_demo/
}

check_if_fix () {
	bash scripts/check_all_stop_types.sh patched_lines_demo/ patch_fixes_demo/
}

truncate_files
complete
build_patches
check_if_fix

mkdir -p demos/"$BUG_ID"
mv *demo/ demos/"$BUG_ID"

