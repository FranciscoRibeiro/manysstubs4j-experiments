#!/bin/sh

#This script is used by complete_all.sh
#Input:
#1. path to file to be completed

FILENAME="$1"

proj="$(cut -d "/" -f2 <<< "$FILENAME")"
name="$(basename "$FILENAME")"
name=${name%.java}
mkdir -p outs/truncated/"$proj"
echo "$FILENAME"
python3.9 -u new_main.py "$FILENAME" > outs/truncated/"$proj/$name.txt"

